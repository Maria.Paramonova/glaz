import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private firestore: AngularFirestore) { }
  form = new FormGroup({
    id_question: new FormControl(''),
    question: new FormControl(''),
    option1: new FormControl(''),
    option2: new FormControl(''),
    criteria: new FormControl(''),
    //completed: new FormControl(false)
  })

  formAnswer = new FormGroup({
    id_question: new FormControl(''),
    option: new FormControl(''),
    age: new FormControl(''),
    gender: new FormControl(''),
    lat: new FormControl(''),
    lon: new FormControl('')

    //completed: new FormControl(false)
  })

  createNewQuestion(data) {
    console.log(data);
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection("questions")
        .add(data)
        .then(res => { }, err => reject(err));
    });
  }

  createNewAnswer(data) {
    console.log(data);
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection("answers")
        .add(data)
        .then(res => { }, err => reject(err));
    });
  }

  getQuestions() {
    console.log("giving questions back");
    return this.firestore.collection("questions").snapshotChanges();
  }

}
