import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  send_new_question(question) {
    return this.store.collection('questions').doc(question).valueChanges().toPromise();
  }

  title = 'glaz-front';

  constructor(private router: Router, private store: AngularFirestore) { }

  ngOnInit(): void {
  }

}
