import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './map/map.component';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { QuestionComponent } from './question/question.component';



const routes: Routes = [

  {
    path: '',
    component: MapComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'question',
    component: QuestionComponent
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
