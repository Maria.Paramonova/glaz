import { Component, OnInit } from '@angular/core';
import { QuestionService } from "../shared/question.service";
import { BehaviorSubject } from 'rxjs';
import { Question } from 'src/app/interfaces/question';
import { Answer } from '../interfaces/answer';
import { FormGroup, FormBuilder } from '@angular/forms';




@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  currentQuestion: BehaviorSubject<Question> = new BehaviorSubject(null);

  answerForm: FormGroup;
  errorMessage: string;
  model: Answer = { id_question: '', option: 'no', age: '15', gender: '', lat: '', lon: '' };

  formSubmitted = false;

  formControlAnswer = {
    id_question: '',
    option: '',
    age: '',
    gender: '',
    lat: '',
    lon: ''
  }

  constructor(private formBuilder: FormBuilder, public questionService: QuestionService) { }




  ngOnInit() { this.getQuestions(); }

  questions: Question[] = [];
  selectedIndex: number = -1;
  answers;

  getQuestions = () => {
    this.questionService
      .getQuestions()
      .subscribe(res => {
        for (let q of res) {
          let question: Question = q.payload.doc.data() as Question;
          question.id = q.payload.doc.id;
          this.questions.push(question)
        }
        console.dir(this.questions);
      });
  }

  onSubmit() {
    if (!this.formSubmitted) {

      this.questionService.createNewAnswer(this.formControlAnswer)
        .then(res => {
          console.log(this.formControlAnswer);
        });
      this.formSubmitted = true;
    }
  }


  setSelectedQuestion(index: number): void {
    this.currentQuestion.next(this.questions[index]);
    this.selectedIndex = index;
  }

  controlSuccessful(): boolean {
    // Check the contents of this.formControl :)
    return true;
  }
}
