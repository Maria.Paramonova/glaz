export interface Question {
    question: string,
    option1: string,
    option2: string,
    criteria: string
    id?: string;
}
