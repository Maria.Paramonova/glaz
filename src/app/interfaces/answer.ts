export interface Answer {
    id_question: string,
    option: string,
    age: string,
    gender: string,
    lat: string,
    lon: string,
    criteria?: string;

}



/*export class Answer {

    constructor(
        public id_answer: number,
        public id_question: number,
        public opinion: {
            1?: string
            2?: string
        },
        public age: number,
        public gender: {
            1?: string
            2?: string
            3?: string
            4?: string
            5?: string
            6?: string
            7?: string
        },
        public location: string
    ) { }
}

*/
