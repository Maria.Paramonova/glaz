import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { fromEvent, Observable, Subscription } from "rxjs";
import { debounceTime } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewChecked {
  @ViewChild('map', { read: ElementRef })
  map: ElementRef;

  @ViewChild('question', { read: ElementRef })
  question: ElementRef;

  @ViewChild('about', { read: ElementRef })
  about: ElementRef;

  private resizeObservable$: Observable<Event>;

  position: string = '0';
  width: number = 0;

  /**
  * La taille d'un élément
  * @deprecated prfer a generic private method
  */
  mapWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  questionWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  aboutWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  private selectedOption: number;




  constructor(private router: Router, private cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    /**
     * Detection of window resize event and call to update the highlight line with a small delay
     * to get accurate values
     */
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeObservable$.pipe(debounceTime(300)).subscribe(evt => {
      this.updateValues();
    });
    //if (window.screen.width > 320) { //
    //  this.mobile = true;
    //}
    //else
    //  this.desktop = true;


  }

  ngAfterViewChecked(): void {
    this.updateValues();
  }

  /**
   * Method used to update the different widths of the text options
   */
  updateValues(): void {
    const updateCheck = this.mapWidth;

    this.mapWidth = this.map.nativeElement.offsetWidth;
    this.questionWidth = this.question.nativeElement.offsetWidth;
    this.aboutWidth = this.about.nativeElement.offsetWidth;


    if (updateCheck == 0 && this.mapWidth != updateCheck) this.toggleState(1); // highlight by default 'Getting started' when the component is initialized
    else this.toggleState(this.selectedOption); // otherwise update the highlight line for the current navigation option selected

    this.cdRef.detectChanges();
  }



  /**
   * Method used to modify the width and position of the highlight line
   * 
   * @param {number} menuOption the navigation option to highlight
   */
  toggleState(menuOption: number) {
    this.selectedOption = menuOption;
    switch (menuOption) {
      case 1:
        this.position = this.map.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.mapWidth;
        break;
      case 2:
        this.position = this.question.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.questionWidth;
        break;
      case 3:
        this.position = this.about.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.aboutWidth;
        break;
      default:
        break;
    }
  }


}
