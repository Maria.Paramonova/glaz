import { Component, OnInit } from '@angular/core';
import { Question } from '../interfaces/question';
import { FormGroup, FormBuilder } from '@angular/forms';
import { QuestionService } from "../shared/question.service";



@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})

export class QuestionComponent implements OnInit {

  questionForm: FormGroup;
  errorMessage: string;


  model: Question = { question: 'Did you have COVID?', option1: 'no', option2: 'yes', criteria: 'were you careful?' };

  formSubmitted = false;

  formControl = {
    question: '',
    option1: '',
    option2: '',
    criteria: ''
  }


  constructor(private formBuilder: FormBuilder, public questionService: QuestionService) { }

  ngOnInit() { this.getQuestions(); }
  questions;
  getQuestions = () => {
    this.questionService
      .getQuestions()
      .subscribe(res => (this.questions = res));
  }

  onSubmit() {
    if (!this.formSubmitted) {

      this.questionService.createNewQuestion(this.formControl)
        .then(res => {
          console.log(this.formControl);
        });
      this.formSubmitted = true;
      this.questionForm.reset();
    }
  }


  controlSuccessful(): boolean {
    // Check the contents of this.formControl :)
    return true;
  }

}


