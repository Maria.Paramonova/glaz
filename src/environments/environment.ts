

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebase: {
    apiKey: "AIzaSyBdCX_jiTJG4zuQtBC_iUcw9yGplCjaTAk",
    authDomain: "glaz-b0587.firebaseapp.com",
    projectId: "glaz-b0587",
    storageBucket: "glaz-b0587.appspot.com",
    messagingSenderId: "330186930501",
    appId: "1:330186930501:web:60f863c0f0055d6802e62c",
    measurementId: "G-876C6YR30L"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
